#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h> 

void error(const char *msg)
{
    perror(msg);
    exit(0);
}

int main(int argc, char *argv[])
{
    int sockfd, portno, n;
    char filePath[30];
    struct sockaddr_in serv_addr;
    struct hostent *server;

    char buffer[256];
    if (argc < 4) {
       fprintf(stderr,"usage %s hostname port file\n", argv[0]);
       fprintf(stderr,"text-file aub\n");
       exit(0);
    }
    strcpy(filePath,argv[3]);
    portno = atoi(argv[2]);
    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    if (sockfd < 0) 
        error("ERROR opening socket");
    server = gethostbyname(argv[1]);
    if (server == NULL) {
        fprintf(stderr,"ERROR, no such host\n");
        exit(0);
    }
    bzero((char *) &serv_addr, sizeof(serv_addr));
    serv_addr.sin_family = AF_INET;
    bcopy((char *)server->h_addr, 
         (char *)&serv_addr.sin_addr.s_addr,
         server->h_length);
    serv_addr.sin_port = htons(portno);
    if (connect(sockfd,(struct sockaddr *) &serv_addr,sizeof(serv_addr)) < 0) 
        error("ERROR connecting");
        
    FILE * fp;
    char file[100000];
    int c;
    int i =0;
    fp = fopen(filePath,"r");
    if (!fp)
    {
      fprintf(stderr," %s file not found\n",filePath);
    }
    else
    {
      fprintf(stderr,"Opened file : %s\n",filePath);
      
      bzero(buffer,256);
      strcpy(buffer,filePath);
      n = write(sockfd,buffer,strlen(buffer));
      if (n < 0) 
           error("ERROR writing to socket");
      bzero(buffer,256);
      n = read(sockfd,buffer,255);
      if (n < 0) 
           error("ERROR reading from socket");
      printf("%s\n",buffer);
      while(1) {
        c =fgetc(fp);
        if( feof(fp)){
            break ;
        }          
        file[i]= c;
        i++;
      }
      printf("%s\n",file);
      fprintf(stderr,"Length: %ld\n",i);
      fclose(fp);
      fprintf(stderr,"close file\n");
    }
    
    bzero(buffer,256);
    strcpy(buffer,file);
    n = write(sockfd,buffer,strlen(buffer));
    if (n < 0) 
         error("ERROR writing to socket");
    bzero(buffer,256);
    n = read(sockfd,buffer,255);
    if (n < 0) 
         error("ERROR reading from socket");
    printf("%s\n",buffer);
    close(sockfd);
    return 0;
}